/* (c) https://github.com/MontiCore/monticore */

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import com.google.common.io.Files;
import guidsl._parser.GUIDSLParser;
import guidsl._symboltable.*;
import guidslcore._ast.ASTCompilationUnit;
import guidslcore._ast.ASTPage;
import guidslcore._ast.ASTPageChildren;
import guidslcore._symboltable.PageSymbol;
import org.antlr.v4.runtime.RecognitionException;

import de.monticore.io.paths.ModelPath;
import de.se_rwth.commons.logging.Log;
import guidsl._symboltable.serialization.GUIDSLScopeDeSer;

import static com.google.common.io.Files.getNameWithoutExtension;

public class GUIDSLTool {

  // locations
  public static final String DEFAULT_SYMBOL_LOCATION = "target/symbols";
  public static final String DEFAULT_MODEL_LOCATION = "src/main/resources/example";

  private static final GUIDSLLanguage lang = new GUIDSLLanguage();
  private static final GUIDSLScopeDeSer deser = new GUIDSLScopeDeSer();
  private static GUIDSLGlobalScope globalScope;
  private static ModelPath modelPath;

  public static void main(String[] args) {
    Log.init();

//    if (args.length != 1) {
//      Log.error("Please specify only one single path to the input model.");
//      return;
//    }
//    String model = args[0];
    String model = "src/main/resources/example/PingPong.gui";

    // setup language infrastructure
    Path models = Paths.get(DEFAULT_MODEL_LOCATION);
    Path symbols = Paths.get(DEFAULT_SYMBOL_LOCATION);
    modelPath = new ModelPath(models, symbols);
    globalScope = new GUIDSLGlobalScope(modelPath, lang);
    
    // parse the model and create the AST representation
    final ASTCompilationUnit ast = parse(model);
    Log.info(model + " parsed successfully!", GUIDSLTool.class.getName());
    
    // setup the symbol table
    GUIDSLArtifactScope modelTopScope = createSymbolTable(lang, ast);

    // store artifact scope
    // TODO: Does not work with current MontiCore version
//    deser.setSymbolFileExtension("autsym");
//    deser.store(modelTopScope, symbols);
//    storeSerializedScopeAsJson(modelTopScope, model);

    // get information about child if present
    Optional<PageSymbol> childOpt = getChild(ast);
    childOpt.ifPresent(automatonSymbol -> Log.info("Found Child Symbol: " + automatonSymbol.getName(), GUIDSLTool.class.getName()));
  }

  public static void storeSerializedScopeAsJson(IGUIDSLScope scope, String model) {
    String serializedScope = deser.serialize(scope);

    Path path = Paths.get(DEFAULT_SYMBOL_LOCATION, getNameWithoutExtension(Paths.get(model).getFileName().toString()) + ".json");
    byte[] strToBytes = serializedScope.getBytes();

    try {
      Files.write(strToBytes, path.toFile());
    } catch(IOException e) {
      e.printStackTrace();
    }
  }

  public static Optional<PageSymbol> getChild(ASTCompilationUnit parentNode) {
    ASTPage page = parentNode.getPage();
    if (page.isPresentPageSettings()) {
      ASTPageChildren children = page.getPageSettings().getPageChildren();
      Optional<PageSymbol> pageSymbol = globalScope.resolvePage(children.getMCQualifiedName(0).getQName());
      return pageSymbol;
    } else {
      return Optional.empty();
    }
  }
  
  public static ASTCompilationUnit parse(String model) {
    try {
      GUIDSLParser parser = new GUIDSLParser() ;
      Optional<ASTCompilationUnit> optAutomaton = parser.parse(model);
      
      if (!parser.hasErrors() && optAutomaton.isPresent()) {
        return optAutomaton.get();
      }
      Log.error("Model could not be parsed.");
    }
    catch (RecognitionException | IOException e) {
      Log.error("Failed to parse " + model, e);
    }
    return null;
  }
  
  public static GUIDSLArtifactScope createSymbolTable(GUIDSLLanguage lang, ASTCompilationUnit ast) {
    GUIDSLSymbolTableCreatorDelegator symbolTable = lang.getSymbolTableCreator(globalScope);
    return symbolTable.createFromAST(ast);
  }
  
}
