package guidsl._parser;

import com.google.common.io.Files;
import de.se_rwth.commons.Names;
import de.se_rwth.commons.logging.Log;
import guidslcore._ast.ASTCompilationUnit;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;

public class GUIDSLParser extends GUIDSLParserTOP {

  /**
   * Besides parsing, this also checks that the filename equals the page name and
   * the package declaration equals the suffix of the package name of the model.
   */
  @Override
  public Optional<ASTCompilationUnit> parse(String fileName) throws IOException {
    Log.info("Parsing..", GUIDSLParser.class.getName());

    Optional<ASTCompilationUnit> ast = super.parse(fileName);
    if (ast.isPresent()) {
      String pathName = Paths.get(fileName).toString();
      String simpleFileName = Files.getNameWithoutExtension(pathName);
      String pageName = ast.get().getPage().getName();
      String packageName = Names.getPackageFromPath(Names.getPathFromFilename(pathName));

      String packageDeclaration = "";
      if (ast.get().isPresentPackage()) {
        packageDeclaration = ast.get().getPackage().getMCQualifiedName().getQName();
      }
      if (!pageName.equals(simpleFileName)) {
        Log.error("0xGUI001 The name of the page " + pageName
            + " is not identical to the name of the file " + fileName
            + " (without its file extension).");
      }
      if(!packageName.endsWith(packageDeclaration)){
        Log.error("0xGUI002 The package declaration " + packageDeclaration + " of the page must not differ from the "
            + "package of the page file.");
      }
    }

    return super.parse(fileName);
  }

}
