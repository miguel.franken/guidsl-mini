/* (c) https://github.com/MontiCore/monticore */
package guidsl._symboltable;

public class GUIDSLLanguage extends GUIDSLLanguageTOP {
  public static final String FILE_ENDING = "gui";

  public GUIDSLLanguage() {
    super("SubAutomaton Language", FILE_ENDING);
  }

  protected GUIDSLModelLoader provideModelLoader() {
    return new GUIDSLModelLoader(this);
  }
}
