package guidsl._symboltable;

import java.util.List;

public class GUIDSLModelLoader extends GUIDSLModelLoaderTOP {

  public GUIDSLModelLoader(GUIDSLLanguage language) {
    super(language);
  }

  @Override
  protected void showWarningIfParsedModels(List<?> asts, String modelName) {
    // Do nothing
  }

}
