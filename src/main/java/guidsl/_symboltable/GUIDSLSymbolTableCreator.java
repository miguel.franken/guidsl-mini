package guidsl._symboltable;

import guidslcore._ast.ASTCompilationUnit;

import java.util.Deque;

public class GUIDSLSymbolTableCreator extends GUIDSLSymbolTableCreatorTOP {

  public GUIDSLSymbolTableCreator(IGUIDSLScope enclosingScope) {
    super(enclosingScope);
  }

  public GUIDSLSymbolTableCreator(Deque<? extends IGUIDSLScope> scopeStack) {
    super(scopeStack);
  }

  @Override
  public GUIDSLArtifactScope createFromAST(ASTCompilationUnit rootNode) {
    GUIDSLArtifactScope scope = super.createFromAST(rootNode);

    if (rootNode.isPresentPackage()) {
      scope.setPackageName(rootNode.getPackage().getMCQualifiedName().getQName());
    }
    return scope;
  }
}
